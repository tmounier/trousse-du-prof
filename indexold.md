# La trousse du Prof débutant en Maths/Sciences

<style>


img[src*='#left'] {
    float: left;
    width=250px;
    height: 250px;
    padding: 10px;

}
img[src*='#right'] {
    float: right;
}
img[src*='#center'] {
    display: block;
    margin: auto;
}

  
    
</style>


![Thomas Mounier, PLP Maths Sciences](https://forge.apps.education.fr/tmounier/maths-sciences/-/raw/main/avatar2.jpg#left ) <div> Bienvenue ! Je suis enseignant en mathématiques et sciences physiques et chimie en Lycée professionnel dans l'académie de Lyon.
    
Cette page a pour objectif de regrouper des outils et des ressources pour aider les enseignants de Maths/sciences dans leur entrée dans le métier.

Je ne suis pas le créateur de toutes les ressources, et ces dernières ne conviendront pas à tout le monde, c'est l'essence même du métier !
</div>
<br>

<div>
    
Ce qui peut fonctionner à un instant $t$ avec une classe et un enseignant peut ne pas fonctionner dans un autre contexte. 
Bonne visite !

</div>


Ce site est en refonte (25/05/2024) pour une gestion plus simple pour le futur ! :-)

## Avant la rentrée

- [Différents diplômes, différents programmes, textes officiels](/1 avant la rentrée/
- Des lectures utiles
- Préparation de ses cours
- Une progression par classe et par discipline
- Obtention du diplôme par l'élève et épreuves Maths / sciences en LP

## Le jour de la rentrée 

- Se présenter
- Liste de matériel
- Le premier cours 
- Les cours suivants
- Quelle calculatrice recommander ?

## Pense bête dans le nouvel établissement

- Environnement numérique
- Environnement matériel
- Vie dans le lycée

## Des outils pour faire des maths 

- Offres calculatrices à destination des enseignants 
- Emulateurs de calculatrices 
- Débuter avec Python au LP
- CAPYTALE
- Geogebra

## Des outils pour faire des sciences 

- Faire des sciences avec un Smartphone
- Ressources numériques pour les Sciences 

## Divers

- Automatismes pour les élèves 
- Rituels de classe 
- Devoirs, Notes, Compétences
- Se préserver
- Outils numériques institutionnels
- Liste d'abbréviations rencontrées dans l'EN

<div>Pour les documents dont je suis concepteur, tout est en CC 0 </div>


<img src="https://forge.apps.education.fr/tmounier/1ere-tex/-/raw/main/cc0.png#center" alt="logo cc0"></p>



<!---

## Titre de niveau 2

On peut également créer des listes :
- Item 1
- Item 2
- Item 3

### Titre de niveau 3

Ou des listes numérotées :
1. Item 1
2. Item 2
3. Item 3

#### Titre de niveau 4

On peut insérer des liens : [Texte du lien](https://forge.apps.education.fr/)

Et des images :

![Légende de la photo](https://picsum.photos/id/17/2500/1667)

##### Titre de niveau 5

On peut également ajouter du code :

```python
print("Hello world!")
```

Ou du texte en ligne `comme ceci`.

###### Titre de niveau 6


> On peut aussi citer du texte.

Et enfin, on peut créer des tables :

| Colonne 1 | Colonne 2 |
|:-:|:-:|
| Ligne 1, Colonne 1 | Ligne 1, Colonne 2 |
| Ligne 2, Colonne 1 | Ligne 2, Colonne 2 |

---

## Syntaxe plus avancée

### Modifications du texte

On peut ++souligner++ du texte, ==surligner== du texte ou ~~barrer~~ du texte.

On peut aussi utiliser des exposants (19^th^) ou des indices (H~2~O)

### Ajouts dans le texte

On peut utiliser des codes pour insérer des emojis : :smile: :+1:

On peut écrire des définitions :

Terme à définir
: Définition du terme

On peut utiliser des notes[^1] de bas de page [^unenote]

[^1]: Ceci est la première note de bas de page.

[^unenote]: Voici la deuxième.

On peut utiliser des boîtes prédéfinies :

:::info
Boîte de contenu de type “Informations”
:::

:::danger
Boîte de contenu de type “Danger”
:::

:::success
Boîte de contenu de type “Succès”
:::



```-->
