---
author: Thomas Mounier
title: Avant la rentrée
---

## Textes officiels


Quelques éléments indispensables pour démarrer dans le métier : 


[Programmes Officiels en mathémathiques pour le LP](https://eduscol.education.fr/1793/programmes-et-ressources-en-mathematiques-voie-professionnelle){ .md-button target="_blank" rel="noopener" }

[Programmes Officiels en Sciences pour le LP](https://eduscol.education.fr/1795/programmes-et-ressources-en-physique-chimie-voie-professionnelle){ .md-button target="_blank" rel="noopener" }

[Liste des groupements par spécialité de BAC](https://eduscol.education.fr/document/25963/download){ .md-button target="_blank" rel="noopener" }

[L'épreuve de DNB Pro pour les 3ème Prépa Métiers](https://voie-pro.web.ac-grenoble.fr/diplome-national-du-brevet-serie-professionnelle){ .md-button target="_blank" rel="noopener" }


## Un peu de lecture

Je vous propose 4 BD pour mettre en perspective 4 phénomènes rencontrés à la rentrée (Adèle Huguet, Julie Blanc, Audrey Murillo, Hélène Veyrac) : 


[Les Fiches de Renseignement](https://comprendreleseleves.ensfea.fr/bande-dessinee/fiche-renseignement/){ .md-button target="_blank" rel="noopener" }
[L'effet Pygmalion](https://comprendreleseleves.ensfea.fr/bande-dessinee/pygmalion/){ .md-button target="_blank" rel="noopener" }
[L'illusion disciplinaire](https://comprendreleseleves.ensfea.fr/illusion-disciplinaire/){ .md-button target="_blank" rel="noopener" }
[Poser le cadre](https://comprendreleseleves.ensfea.fr/bande-dessinee/poser-cadre/){ .md-button target="_blank" rel="noopener" }

 
Pour moi ces BD sont très pertinentes dans la manière d'aborder les élèves de lycée professionnel. Extrait de : [https://comprendreleseleves.ensfea.fr/bande-dessinee/](https://comprendreleseleves.ensfea.fr/bande-dessinee/){:target="_blank"}

## Préparation de ses cours 

A ce stade un seul conseil : **doucement sur la préparation**. En effet, il vaut mieux prévoir des activités et séances courtes et pas trop nombreuses tant qu'on ne connait pas ses élèves.

- Préparer une ou deux séances par classe pour mettre les élèves au travail;
- Prévoir de petites activités plutôt qu'un projet trop ambitieux pour démarrer;
- Se prévoir du temps pour adapter / préparer après la première séance.
- Ne pas hésiter à "jeter" une partie de vos préparations que vous estimerez pas adaptées ! 



## Une progression par classe et par discipline

Par définition, une bonne progression (quand on débute) est une progression à refaire ! Donc pas besoin de partir directement sur une progression complexe et annuelle à mon sens : 

- On peut commencer par un trimestre;
- On ajuste, c'est normal
- On ne prévoit pas trop serré !

Utiliser une progression c'est comme faire de la plongée avec un fil d'ariane, elle ne remplace pas notre travail mais elle a un petit côté rassurant. Avec les années, la progression sera moins détaillée.




## Conditions d'obtention de diplôme en LP et épreuves m/sc


Dans la plupart des cas, vos élèves seront évalués (dans nos matières) en CCF : Contrôle en Cours de Formation. L'enseignant aura à charge de rédiger le sujet, de gérer la passation et la notation.

Il n'y a pas d'épreuves finale ! (Sauf si votre établissement n'est pas habilité à proposer les CCF dans ce cas vos élèves passeront une épreuve ponctuelle).


??? tip "Le CCF c'est"
	- D'évaluer en théorie un élève quand il est prêt (dans la réalité, les contraintes matérielles mitigent ce point);
	- De choisir les parties du programme sur lesquelles évaluer (pas de stress de ne pas avoir terminé l'ensemble du programme);


??? warning "Mais le CCF c'est aussi"
	- Un gros travail de création du sujet;
	- Des heures passées à faire passer les épreuves;
	- Un système délicat par rapport à la difficulté du sujet : évaluer objectivement par rapport aux attentes des programmes est une tâche difficile.



Une infographie réalisée par l'équipe d'inspecteurs de l'académie de Lille ( Emelyne DE JAEGHERE et Ludovic DIANA) qui présente les différentes situations d'examen en mathématiques/physique chimie : 

<p style="text-align: center;"> <embed src=https://pedagogie.ac-lille.fr/maths-physique-chimie/wp-content/uploads/sites/3/2023/08/CCF_2024pdf.pdf width=1000 height=750 type='application/pdf'/></p>

