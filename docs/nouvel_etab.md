---
author: Thomas Mounier
title: Mon nouvel établissement
---

L'affectation est tombée. Vous êtes affecté(e) dans le lycée professionnel Machin dans la belle ville de Trifouille les Oies. 
Passé le premier choc, vous avez trouvé un logement, et vient le moment du contact et de la rentrée. Alors, comment ça se passe dans la réalité ?


<p style="text-align: center;"> <img src="../images/lycee.png" alt="Girl in a jacket" width="500" height="600"> </p>

## Les périodes d'ouverture

Selon la date de votre nomination, il peut être intéressant de contacter son établissement rapidement. 

??? tip "Voir pourquoi"
	1. Se présenter au chef
	2. Participer à d'éventuelles réunions d'équipe disciplinaire de fin d'année;
	3. Connaître ses classes et niveaux
	4. Connaître la date de réouverture fin aout 
	
En général les établissements fonctionnent ainsi l'été : 

- Ouverts jusqu'aux alentours du 10 juillet : il est probablement possible d'avoir quelqu'un au téléphone sur place;
- Fermés entre le 10 juillet et le 25 aout (ces dates sont indicatives); il sera compliqué d'avoir quelqu'un. 
- Réouverture fin aout autour du 25 pour la rentrée. 

!!! info "Une astuce de contact"
	Les établissements d'enseignement publics ont tous une adresse mail du chef d'établissement en général consultée.
	Cette adresse est sur le modèle :  **ce.UAI@ac-académie.fr**
	UAI étant un code spécifique à chaque établissement. 
	Un [Moteur de recherche](https://data.education.gouv.fr/explore/dataset/fr-en-annuaire-education/table/?disjunctive.nom_etablissement&disjunctive.type_etablissement&disjunctive.type_contrat_prive&disjunctive.code_type_contrat_prive&disjunctive.pial&disjunctive.appartenance_education_prioritaire&disjunctive.code_postal&disjunctive.nom_commune&disjunctive.code_departement&disjunctive.libelle_academie&disjunctive.libelle_region&disjunctive.ministere_tutelle){:target="_blank"} existe.
	Penser à remplacer "académie" par votre académie. 
	

## J'y suis, je fais quoi ?

Vous voila devant la grille, voici un petit pense bête des choses à faire avant la rentrée : 

### Le côté matériel

Un petit tableau sous la forme de check liste de choses à vérifier en arrivant (cliquez pour dérouler)

??? tip "Ma liste matérielle"
	|    Check liste | 
	| :---   | 
	| Clés/Badges   |   
	| Visite établissement |  
	| Pôle Maths/sciences |  
	| Matériel de sciences  |  
	| Feutres, fournitures tableau |  
	| Cantine ? |  
	| Qui contacter travaux ? |  
	| Qui contacter informatique ? |  
	| Fonctionnement de la vie scolaire | 
	| Fonctionnement de l'infirmerie  |  

### Le côté numérique

Ici encore une petite liste de choses à vérifier avant la rentrée, cliquez pour afficher.
??? tip "Ma liste Numérique"
	|    Check liste | 
	| :---    | 
	| Codes pour allumer un ordinateur   |   
	| Codes pour imprimer / Photocopier |  
	| Codes pour logiciel de vie scolaire |  
	| Codes pour accéder aux services en ligne (ENT...)  |  
	| Qui est la personne ressource de chaque outil numérique |  
	| Règles en matière de communication interne ? |  
	| Règles en matière de communication élèves/familles ? |  
	| Qui contacter en cas de problème informatique ? |  

Remarque : le portail unique de connexion basé sur l'identité académique des agens est de plus en plus utilisée. Chaque agent possède un compte sur les outils académiques qui permet de se connecter à un bouquet de services. 


### Les acteurs de l'établissement 

Ils sont multiples, et sont ressources pour un nouvel arrivant. 

- La vie scolaire / CPE pour la gestion des classes; Procédures pour faire l'appel, les punitions, sanctions, rapports ...
- Le carnet de correspondance ? Est-il papier, numérique ?
- Le règlement intérieur
- La salle des profs pour s'intégrer en équipe mais pas que;
- Les services gestion & intendance; 
- Les différents secrétariats 
- La direction

