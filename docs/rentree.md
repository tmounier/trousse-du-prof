---
author: Thomas Mounier
title: Jour de la rentrée
---

## Présentation


!!! info "Un conseil"
	On essaye de passer dans toutes les classes pour se présenter dans la mesure du possible.  

Pas besoin de raconter sa vie, "Bonjour, je suis M. Truc, je serai votre professeur de mathématiques pour cette année. Si vous avez vu votre EDT, nous nous retrouverons pour une première séance le XX à XXheures".


## Une liste de matériel 

On garde en tête que les familles ont peut être déjà reçu une liste par l'établissement pour votre(vos) matière(s) et on s'adapte dans la mesure du possible.

- La liste doit être simple (on évitera par exemple un cahier grand carreaux 24x32 de couleur magenta et de 97 pages)
- On laisse du temps pour les achats;
- Mais pas trop de temps ! 
- On publie la liste aux élèves, aux parents, via le cahier de texte ... Qu'elle touche tout le monde dans la famille !


## Un premier cours

Lors de votre premier cours, la première impression sera très importante. Il faut selon moi mettre les élèves au travail le plus vite possible.


??? tip "Ma séance type (cliquez pour dérouler)"
	1. Je fais l'appel, en prenant mon temps;
	2. Je pose le cadre, rapidement, avec un document SIMPLE
	- Je refais l'appel;
	- Je présente l'année et les éventuelles échéances en quelques minutes;
	- Je propose immédiatement une activité **brise glace** : l'élève est à l'aise pour tenter, possiblement en petits groupes pour se découvrir, et discuter ensuite ensemble.
	- Je termine par un rappel sur la prochaine séance (fournitures, travail à finir...) pour reprendre la main avant la fin.


!!! danger "Danger : Pourquoi je déconseille la fiche de renseignement"
	[Relire la BD à ce sujet](https://comprendreleseleves.ensfea.fr/bande-dessinee/fiche-renseignement/){ .md-button target="_blank" rel="noopener" }
	
	
## Les cours suivants

Si la première séance est cruciale, les suivantes sont aussi importantes pour intégrer une dynamique de travail. 

Quelques conseils / remarques : 

- A chaque fois, **mettre rapidement les élèves au travail** (instaurer un rituel de début de séance);
- Le **cadre de travail doit être simple** pour qu'il soit simple à appliquer et ne se transforme pas en usine à gaz intenable;
- Il faut essayer de **placer les élèves dans une atmosphère de travail et de confiance** pour les inciter à oser se tromper. 
- La première partie de l'année va être une phase de test pour la classe et pour l'enseignant. Courage !



## Quelle calculatrice recommander ?

Ce sujet est délicat et plusieurs facteurs sont à prendre en compte : 

- Quelle est la politique de l'équipe disciplinaire ? Si oui, on s'y plie quitte à discuter l'année d'après.
- Les familles ont-elles déjà eu une information avec un modèle ? Si oui, on s'y plie pour éviter le double achat impossible.
- Le budget : même si elles n'en parlent pas, certaines familles seront en difficulter à acheter une calculatrice, même peu onéreuse. On peut diriger vers le fond social de l'établissement.

Il appartient ensuite à chaque enseignant de se faire sa propre idée et de recommander un modèle. 

- En CAP la calculatrice collège suffit largement;
- En BAC il peut être intéressant de réfléchir à la poursuite post bac avant de recommander. 
