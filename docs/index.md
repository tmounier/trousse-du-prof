# A propos de ce site


    
</style>

![Thomas Mounier, PLP Maths Sciences](https://forge.apps.education.fr/tmounier/maths-sciences/-/raw/main/avatar2.jpg ){ width=28%; align=left } 

Bienvenue ! Je suis enseignant en mathématiques et sciences physiques et chimie en Lycée professionnel dans l'académie de Lyon.
    
Cette page a pour objectif de regrouper des outils et des ressources pour aider les enseignants de Maths/sciences dans leur entrée dans le métier.

Je ne suis pas le créateur de toutes les ressources, et ces dernières ne conviendront pas à tout le monde, c'est l'essence même du métier !

    
Ce qui peut fonctionner à un instant t avec une classe et un enseignant peut ne pas fonctionner dans un autre contexte. 
Bonne visite !



<br>
<br>


Ce site est hébergé sur [La Forge](https://forge.apps.education.fr/){:target="_blank"} des communs numériques éducatifs et créé via [un modèle](https://tutoriels.forge.apps.education.fr/mkdocs-pyodide-review//#les-sites-modeles){:target="blank"}. 



<p xmlns:cc="http://creativecommons.org/ns#" >Les ressources sont déposées - quand elles sont de ma propre création - sous licence <a href="https://creativecommons.org/publicdomain/zero/1.0/?ref=chooser-v1" target="_blank" rel="license noopener noreferrer" style="display:inline-block;">CC0 1.0<img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/cc.svg?ref=chooser-v1" alt=""><img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/zero.svg?ref=chooser-v1" alt=""></a></p> 