---
author: Thomas Mounier
title: Des outils pour les Maths
---

Dans cette rubrique des outils ou astuces pour l'enseignement des mathématiques

## Offres Calculatrices et émulateurs pour les enseignants

Quand vous disposerez d'une adresse académique la plupart des éditeurs proposent (les offres varient au cours du temps) des prêts (longue durée) de leurs modèles.

- [Numworks](https://www.numworks.com/fr/professeurs/offres/){:target="_blank"} : Offre de prêt 
- [Casio](https://www.casio-education.fr/tarifs-enseignants/){:target="_blank"} : Tarifs enseignants (offre de prêt en avril 2024 mais probablement expirée)
- [Texas Instruments](https://education.ti.com/fr/acheter/tarif-enseignant){:target="_blank"} : Tarifs enseignants 

De manière plus générale ce site est une mine d'or sur ce thème : [TI planet](https://tiplanet.org/forum/portal.php){ .md-button target="_blank" rel="noopener" }

!!! warning "Remarque" 
	Il faut garder en mémoire qu'il existe peut être une politique d'équipe quant à la calculatrice. 
	Il est aussi très compliqué en LP d'avoir 100% d'une classe équipée (pour ne pas dire impossible
	

??? tip "Mon avis"
	Qui n'engage que moi !
	Depuis que mes élèves ont découvert Numworks avec l'appli Smartphone ou l'émulateur, impossible de revenir en arrière.
	!!! info "Une idée"
		Et pourquoi pas proposer l'achat d'un lot à prêter aux élèves par le lycée ?
		

## Débuter Python au lycée pro 

Depuis 2019 les programmes imposent de travailler le langage Python dans le cadre de l'algorithmie et de la programmation. 

Des ressources existent en ligne pour se former. Une pépite à utiliser plutôt pour les enseignants (mais on peut envisager de lancer des élèves très volontaires dessus) : 



[PYrates](https://py-rates.fr/){ .md-button target="_blank" rel="noopener" }


J'ai aussi rédigé une ressource d'aide pour les collègues débutant Python que vous trouverez en téléchargement [ici](a_telecharger/book_python.pdf){:target="_blank"}

!!! info "CAPYTALE"
	Pour travailler avec les élèves pour moi l'enseignant aurait tout intérêt à utiliser [CAPYTALE](https://capytale2.ac-paris.fr/web/accueil){:target="_blank"}
	En effet cet espace : 
	
	- Est institutionnel;
	- S'interconnecte avec la plupart des ENT pour les élèves;
	- Travaille de n'importe où vu qu'on ne doit rien installer.
	- Permet la mutualisation des ressources entre collègues;
	- Permet d'ouvrir les productions élèves sans manipulation de fichiers.
	Ici aussi, je vous propose une [ressource](a_telecharger/capytale_lp.pdf){:target="_blank"} rédigée par mes soins.

??? tip "consulter le fichier Python depuis la page"
    <div class="centre">
    <iframe 
    src="../a_telecharger/book_python.pdf"
    width="1000" height="1000" 
    frameborder="0" 
    allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
    </iframe>
    </div>

??? tip "consulter le fichier Capytale depuis la page"
    <div class="centre">
    <iframe 
    src="../a_telecharger/capytale_lp.pdf"
    width="1000" height="1000" 
    frameborder="0" 
    allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
    </iframe>
    </div>
	

## Geogebra

Pour les collègues, cet outil est une "évidence" mais pour les nouveaux : [Geogebra](https://www.geogebra.org/){:target="_blank"} est un logiciel à la base pour représenter des graphes qui contient maintenant beaucoup plus de fonctionnalités.

- Représenter des fonctions;
- Résoudre des équations;
- Faire des statistiques (à 1 ou 2 variables)
- Travailler en mode "Class room" (avec les problématiques de création de comptes)

Il est possible d'utiliser Geogebra en classe au tableau, pour ma part, je ne fais plus travailler mes élèves dessus. Je préfère la calculatrice ou des logiciels EXAO ou directement Tableur.
