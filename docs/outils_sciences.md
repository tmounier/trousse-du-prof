---
author: Thomas Mounier
title: Des outils pour les Sciences
---

Cette page a pour objectif de recenser des ressources. Cette liste est incomplète, il existe un nombre incroyable de ressources. 

## Faire des sciences avec un Smartphone

!!! danger "Attention"
	Le Smartphone peut être un sujet sensible dans votre établissement. 
	Il est recommandé de se renseigner sur les pratiques en amont.
	Se rapprocher du Règlement Intérieur et du Chef d'Etablissement en cas de doute
	Attention aux vols et risques pour les appareils.
	

Les Smartphone des élèves contiennent une multitude de capteurs permettant de faire des sciences. 

Une application développée pour faire de la physique est : [FizziQ](https://www.fizziq.org/){ .md-button target="_blank" rel="noopener" }

Une banque de ressource d'activité rangées par thème se trouve [à cet endroit](https://www.fizziq.org/protocoles){:target="_blank"} pour accompagner les enseignants.


## Des simulations et ressources numériques


L'académie de Toulouse recense sur [cet espace](https://pedagogie.ac-toulouse.fr/mathematiques-physique-chimie/outils-numeriques-pour-la-physique-chimie){:target="_blank"} un nombre important d'outils numériques pour les sciences.