---
author: Thomas Mounier
title: Remerciements
---

Le site est hébergé par la forge de [Apps Education](https://forge.apps.education.fr/).

Le site est construit avec [`mkdocs`](https://www.mkdocs.org/) et en particulier [`mkdocs-material`](https://squidfunk.github.io/mkdocs-material/).



