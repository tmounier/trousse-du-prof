---
author: Thomas Mounier
title: Outils Numériques Institutionnels
---

Notre employeur (le ministère de l'éducation nationale) met à disposition de ses agents un large évantail d'outils, parfois méconnus, allant de l'indispensable à l'utile. 

!!! danger "Attention"
	Il faut éviter - dans la mesure du possible - d'utiliser un outil non institutionnel dans le cadre de sa mission. Principalement dans un souci de protection des données des utilisateurs.
	

Ce document essaye de résumer une partie des outils et de les décrire :

## Le portail ARENA

Ce portail centralise beaucoup d'outils et dépend de votre académie d'exercice (exemple pour Lyon : https://portail.ac-lyon.fr/arena/pages/accueill.jsf ).
Les outils principaux accessibles via ce portail sont : 

- Votre adresse académique personnelle via un client en ligne de messagerie (webmail). Cette adresse est du type prenom.nom@ac-académie.fr . La DRANE de Lyon met à disposition sur son site une page dédiée à l'utilisation de la [messagerie académique](https://drane.ac-lyon.fr/spip/Messagerie-academique){:target="_blank"}

??? warning "Important concernant la messagerie académique"
	Sa consultation est obligatoire. Les services des pluparts des académies ne traitent plus les demandes qui proviennent d'une adresse privée. 

- Iprof : c'est l'interface de suivi de carrière de l'enseignant. Son design "moderne" (du début des années 2000) fait peur mais c'est par ce biais que se traiteront les futures demandes de mutation par exemple. 
- Educonnect - Dépannage des comptes élèves. Si votre chef d'établissement vous autorise à utiliser ce service, vous le retrouverez dans Arena Lyon 
- DT Chorus - Pour la gestion des déplacements et du remboursement des frais 
- Imagin - Pour les convocations d'examen (en train d'être remplacé)
- Magistère : Plateforme de formation en ligne de l'éducation nationale (v2 arrive rentrée 2024 avec la disparition du @ )
- Colibri : Le portail agent RH (remboursement d'une partie de la mutuelle par exemple)
- Annuaire académique des agents 
- Intranet académique 


## Le portail Apps-Education

Depuis le confinement, des efforts ont été déployés dans la mise en place d'outils numériques permettant de travailler et ont été centralisés dans la plateforme [Apps Education](https://portail.apps.education.fr/signin){:target="_blank"}

Les services proposés dans Apps ont pour but de fournir un espace institutionnel pour éviter des solutions grand public problématiques pour la gestion des données 

- Nuage : un espace de stockage en ligne (cloud) avec 100GO par adresse académique. Possibilité d'utiliser une application mobile, en mode web ou en client. 
- [Tubes](https://tubes.apps.education.fr/){:target="_blank"} : une plateforme thématique de stockage de vidéos;
- [La Forge](https://forge.apps.education.fr/){:target="_blank"} des communs numériques éducatifs : Créer et partager du code et du contenu pédagogique (ce site est hébergé sur la forge et créé à partir d'un modèle présent sur la forge)
- [CodiMD](https://codimd.apps.education.fr/){:target="_blank"} : un éditeur (pouvant être collaboratif) en markdown pour rédiger des documents, créer des diaporamas, travailler à plusieurs mains. 
- [Tchap](https://www.tchap.gouv.fr/){:target="_blank"} (présent dans apps mais commun aux ministères) : une messagerie instantanée et sécurisée des agents (version web et mobile)
- [Visio Agents](https://visio-agents.education.fr/welcome){:target="_blank"} : un service de web-conférence pour les agents

Un des avantages de Apps Education est la stabilité du compte même en cas de mutation professionnelle. 

??? tip "Astuce"
	Pour un utilisateur ayant l'habitude d'utiliser gmail, gdrive et whatsapp, les outils ci-dessus remplacent sans problème ces usages et de manière conforme aux attentes de sécurité.