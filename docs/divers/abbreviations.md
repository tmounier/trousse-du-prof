---
author: Thomas Mounier
title: Abbréviations du monde de l'EN
---

Je vais te donner ton NUMEN qui te permettra de te connecter à l'ENT et ensuite tu iras voir un AED pour qu'il t'explique le fonctionnement des retenues. N'oublies pas de passer voir sur le site de tes IEN pour les modalités de CCF pour l'année en cours.

<p style="text-align: center;"><span style='font-size:75px;'>&#128552;</span></p>

Cette liste d'abbréviations est non exhaustive et "classée" par "thèmes" arbitraires. Certaines peuvent être en doublon dans plusieurs catégories.



## Le prof 

- HSA : Heure Supplémentaire Année
- HSE : Heure Supplémentaire Effective

- NUMEN : NUméro d'identification de l'Education Nationale (donnée personnelle)

- PP : Professeur Principal

- RGPD : Règlement Général sur la Protection des Données

- TICE : Technologie d'Information et de Communication
- TZR : Titulaire sur Zone de Remplacement (en voie de disparition au profit de contractuels)

## La vie scolaire et de l'élève

- AED : Assistance d'éducation (ex : surveillant)
- AESH : Accompagnant d'élève en situation de handicap

- CDI : Centre de Documentation et d'Information
- CIO : Centre d'Information et d'Orientation
- CPE : Conseiller Principal d'Education
- CVL : Conseil de la Vie Lycéenne
- CCF : Contrôle en Cours de Formation


- ENT : Espace Numérique de Travail (peut aussi désigner le site web de l'établissement)

- PAI : Projet d'Accueil Individualisé
- PAP : Plan d'Accompagnement Personnalisé
- PIX : Pas vraiment une abbréviation, nom de la certification portant sur la maîtrise du numérique 

- SEGPA : Section d'Enseignement Général et Professionnel Adapté

- ULIS : Unité Localisée pour l'Inclusion Scolaire
- UNSS : Union Nationale du Sport Scolaire 


## Les diplômes et épreuves

- BTS : Brevet de Technicien Supérieur (bac +2)
- BP : Brevet Professionnel (bac)
- BEP : Brevet d'Etudes professionnelles (n'existe plus)

- CAP : Certificat d'Aptitude Professionnelle
- CCF : Contrôle en Cours de Formation


- DNB : Diplôme National du Brevet 
- DNL : Discipline Non Linguistique (certificat pour l'enseignant voulant travailler en classe européenne)

- FCIL : Formation Complémentaire d'Initiative Locale

- GRETA : GRoupement d'ETablissements pour la formation professionnelle continue (apprentissage) 

## L'institution

- BIR : Bulletin d'information rectoral
- BO : Bulletin Officiel

- DRANE : Délégation Régionale Académique au Numérique Educatif

- EAFC : Ecole Académique de la Formation Continue (pour les enseignants)
- EPLE : Etablissement Public Local d'Enseignement (collège, lycée)

- IEN : Inspecteur de l'Education Nationale

- REP : Réseau d'Education Prioritaire
- RGPD : Règlement Général sur la Protection des Données

- ZEP : Zone d'Education Prioritaire 

## La vie de l'établissement

- AS : Assistante Sociale
- ATOSS : Administratif, Technicien, Ouvrier de Service et de Santé et Sociaux (personnels des établissements, agents)

- CA : Conseil d'Administration
- CVL : Conseil de la Vie Lycéenne

- DHG : Dotation horaire globale (l'enveloppe d'heures qui détermine le nombre de postes dans l'établissement)

- ENT : Espace Numérique de Travail (peut aussi désigner le site web de l'établissement)

- GRETA : GRoupement d'ETablissements pour la formation professionnelle continue (apprentissage) 

- HSA : Heure Supplémentaire Année
- HSE : Heure Supplémentaire Effective

- MLDS : Mission de Lutte contre le Décrochage Scolaire

- PPMS : Plan Particulier de Mise en Sureté

- SEGPA : Section d'Enseignement Général et Professionnel Adapté

- UAI : Unité Administrative Immatriculée (numéro de l'établissement)
- ULIS : Unité Localisée pour l'Inclusion Scolaire


## L'enseignement maths / sciences

- CCF : Contrôle en Cours de Formation

- EXAO : EXpérimentation Assistée par Ordinateur

- HSA : Heure Supplémentaire Année
- HSE : Heure Supplémentaire Effective

- TICE : Technologie d'Information et de Communication
