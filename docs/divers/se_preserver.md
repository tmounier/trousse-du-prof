---
author: Thomas Mounier
title: Préserver sa santé
---

L'entrée dans le monde de l'éducation est souvent synonyme de course contre la montre. 
Une de vos priorités principales durant cette première année va être de préserver votre esprit et votre corps pour tenir sur la durée.

Vous n'avez pas signé pour une course de quelques kilomètres mais plutôt pour un marathon, alors préservons nous ! 

Les problèmes récurrents qui nous "brûlent" : 

- Un sentiment de ne jamais être content de sa séance;
- Le sentiment de l'imposteur;
- Se dire qu'avec plus de temps, tout aurait été plus simple (spoiler : non)
- Se dire que c'est plus simple pour les autres collègues (spoiler : non) 

!!! info "Se prendre du temps pour soi"
	Il est vital de ne pas penser que Education Nationale pendant cette première année et de couper à un moment de la semaine.
	
	
![paysage](../images/zen.jpg){ width=50%; : .center }

