---
author: Thomas Mounier
title: Travailler les automatismes
---



## Automatismes

### C'est quoi ? 

Dans les nouveaux de programmes ils correspondent à des actions répétées et brèves ciblées sur un savoir faire.

Exemples : calculer un pourcentage, utiliser la calculatrice pour calculer un logarithme ...



### Pourquoi ?

Plusieurs raisons : 

- Il n'y a qu'en répétant que les élèves gravent en mémoire et savent faire ce genre d'actions;
- Pour lever la difficulté "opératoire" et se concentrer à un autre moment sur le résultat et son sens;
- Comme un [rituel](rituels.md) de début d'heure pour instaurer le calme et mettre au travail.



### Des idées ?

Quelques pistes à adopter : 

- Des activités "Flash" en papier : 5 minutes, 5 questions 
- Du calcul mental (via par exemple un outil externe comme [jepeuxpasjaimaths](https://www.jepeuxpasjaimaths.fr/){:target="_blank"}
- Des diaporamas projetés avec des questions rapides; (suggestion : [MathALEA](https://coopmaths.fr/alea/){:target="_blank"})
- Des activités données dans ELEA (plus difficile pour du début de séance)
- Des sondages rapides où l'enseignant scanne les cartes (suggestion : [QCMcam](https://qcmcam.net/){:target="_blank"})