---
author: Thomas Mounier
title: Rituels en classe
---

Par rituel on entend une action ou un temps d'une séance "figés" et de courte durée : les élèves savent quoi faire et ce qu'il va se passer.

!!! info "Un exemple"
	Le rituel d'entrée dans la classe, rester debout derrière sa chaise, attendre.
	
Mettre en place un ou plusieurs rituels est conseillé surtout quand on démarre. 

!!! tip "Intérêts"
	
	- Poser un cadre rassurant;
	- Récupérer l'attention des élèves;
	- Conclusion d'une séance avec par exemple écriture d'une phrase "trace écrite"
	
La fin du cours est aussi un moment important, et ce rituel de "tout le monde dehors" peut être contrôlé par l'enseignant.

Les rituels se couplent très bien avec les [automatismes](automatismes.md)

