---
author: Thomas Mounier
title: Devoirs, Notes et compétences
---

## Les 5 compétences 

Lors de l'examen final (CCF ou épreuve ponctuelle) nos élèves sont évaluées selon la grille des 5 compétences des mathématiques et des sciences.

??? tip "Découvrir les 5 compétences"
	
	- C1 : S'approprier
	- C2 : Analyser / Raisonner
	- C3 : Réaliser
	- C4 : Valider
	- C5 : Communiquer
	

??? info "Cliquer ici pour un exemple de grille d'évaluation"
	<div class="centre">
    <iframe 
    src="https://mpc.web.ac-grenoble.fr/sites/default/files/Media/document/grille_eval_v15.pdf"
    width="1000" height="1000" 
    frameborder="0" 
    allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
    </iframe>
    </div>

## Evaluer par compétences 

Il n'est pas toujours possible d'évaluer les élèves avec cette grille. En revanche, introduire dès le début les compétences permet une transition plus douce. 
On peut rajouter dans ses devoirs des petites icônes pour chaque compétence si on veut signaler celle qui domine un exercice ou une question.


## Différents types d'évaluation 

On peut jongler entre différentes types d'évaluations : 

- L'évaluation rapide notée sur barème;
- L'évaluation rapide qui évalue une compétence précise;
- Un devoir avec des exercices classiques noté avec un barème ou par compétence;
- Un devoir avec la double notation;
- Une évaluation de "type" CCF notée par compétence et dans le même esprit.

??? tip "Ma pratique"
	**C'est imparfait !** 
	J'évalue les élèves avec des devoirs classiques et des devoirs type CCF (de plus en plus à l'approche de la fin de première bac par exemple)
	